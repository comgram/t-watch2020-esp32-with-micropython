
I recently got a Lilygo t-watch-2020 with esp32,
and i want to use it with Micropython. While it was
easy to list the i2c peripherals, getting the display
to work was not that trivial since it needed the
AXP202 - power managment chip setup in the right way.

# how to use it:

- import lily
- li=lily.LILY()
- li.testimg()
- li.paint()


For more Info: see the wiki:

  https://gitlab.com/mooond/t-watch2020-esp32-with-micropython/-/wikis/home

